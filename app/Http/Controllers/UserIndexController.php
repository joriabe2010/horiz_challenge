<?php

namespace App\Http\Controllers;

use App\Models\HorizmUser;

class UserIndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $users = HorizmUser::all()->sortByDesc('avg_rating')?->makeVisible('posts');

        if($users){        
            return response($users, 200)
                ->header('Content-Type', 'application/json');
        }

        return response('User Data Not found', 404)
            ->header('Content-Type', 'text/plain');          
    }
}
