<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class ShowPostController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $post_id)
    {
        $post = Post::find($post_id)?->makeVisible('user_name')?->makeHidden('user_id');

        if($post){        
            return response($post, 200)
                ->header('Content-Type', 'application/json');
        }

        return response('Post Not found', 404)
            ->header('Content-Type', 'text/plain'); 
    }
}
