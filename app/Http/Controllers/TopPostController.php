<?php

namespace App\Http\Controllers;

use App\Models\HorizmUser;

class TopPostController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $users = HorizmUser::all();

        $posts = $users->map(function ($user, $key) {
           return $user->posts->sortByDesc('rating')->first()?->makeVisible(['rating','user_name']);
        });

        if($posts){        
            return response($posts, 200)
                ->header('Content-Type', 'application/json');
        }

        return response('Post Data Not found', 404)
            ->header('Content-Type', 'text/plain');
    }
}
