<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use App\Models\Post;
use App\Models\HorizmUser;


class HorizmUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $response = Http::get('https://jsonplaceholder.typicode.com/users');
        
        $response->collect($key = null)->each(function (array $item, int $key) {

            if(Post::where('user_id', $item['id'])->first()){
                HorizmUser::create([
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'email' => $item['email'],
                    'city' => $item['address']['city'],
                ]);
            }            
        });
    }
}
