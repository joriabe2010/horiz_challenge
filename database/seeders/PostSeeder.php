<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $response = Http::get('https://jsonplaceholder.typicode.com/posts');
        
        $response->collect($key = null)->take(50)->each(function (array $item, int $key) {
            Post::updateOrCreate(
                ['id' => $item['id']],
                [
                    'user_id' => $item['userId'], 
                    'title' => $item['title'], 
                    'body' => $item['body'], 
                    'rating' => $this->getPostValue($item['title'], $item['body'])
                ]
            );
        });
    }

    private function getPostValue(string $postTitle, string $postBody){
        return (str_word_count($postTitle) * 2) + str_word_count($postBody);
    }
}
