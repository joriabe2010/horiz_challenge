<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title =  $this->faker->sentence(random_int(2, 5));
        $body =  $this->faker->paragraph(3);
        
        return [
            "user_id" => 1,
            "title" => $title,
            "body" => $body,
            "rating" => $this->getPostValue($title, $body),
        ];
    }

    private function getPostValue(string $postTitle, string $postBody){
        return (str_word_count($postTitle) * 2) + str_word_count($postBody);
    }
}
