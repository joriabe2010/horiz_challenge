<?php

use App\Http\Controllers\PostsApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserIndexController;
use App\Http\Controllers\TopPostController;
use App\Http\Controllers\ShowPostController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users', UserIndexController::class)->name('users');
Route::get('/posts/top', TopPostController::class)->name('posts.top');
Route::get('/posts/{id}', ShowPostController::class)->name('posts.show');




