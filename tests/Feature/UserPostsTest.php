<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\HorizmUser;
use App\Models\Post;


class UserPostsTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * Test that the API returns a list of users with posts.
     *
     * @return void
     */
    public function test_api_get_user_with_post()
    {

        HorizmUser::factory()->count(3)
        ->hasPosts(4)->create();

        $users = HorizmUser::all()?->makeVisible('posts');

        $response = $this->getJson('/api/users');

        $response->assertStatus(200);

        $response->assertJson(
            $users->toArray()
        );
    }

    /**
     * Test that the API returns a list with top post for each user.
     * 
     * @return void
     */
    public function test_api_get_posts_top_with_user()
    {
        HorizmUser::factory()->count(2)
        ->hasPosts(3)->create();

        $users = HorizmUser::all();

        $posts = $users->map(function ($user, $key) {
            return $user->posts->sortByDesc('rating')->first()?->makeVisible(['rating','user_name']);
        });

        $response = $this->getJson('/api/posts/top');

        $response->assertStatus(200);

        $response->assertJson(
            $posts->toArray()
        );
    }

    /**
     * Test that the API returns a list of users with posts.
     * 
     * @return void
     */
    public function test_api_get_post_show()
    {

        HorizmUser::factory()->count(1)
        ->hasPosts(3)->create();

        $post = Post::first()?->makeVisible('user_name')?->makeHidden('user_id');        

        $response = $this->getJson('/api/posts/'.$post->id);

        $response->assertStatus(200);

        $response->assertJson(
            $post->toArray()
        );

        $response_fail = $this->getJson('/api/posts/99');

        $response_fail->assertStatus(404);
    }
}