## Instrucciones
    1.	Clonar repositorio.	
    2.	Ejecutar comandos:
        a.	 ‘composer install’
        b.	‘php artisan migrate’
        c.	‘php artisan db:seed’
    3.	Probar los diferentes endpoints:
        a.	/api/users
        b.	/api /posts/top
        c.	/api/posts/{id}
    4.	Ejecutar Test ‘php artisan test’


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
